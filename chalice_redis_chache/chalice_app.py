from datetime import timezone
import datetime
import jinja2
from chalice import Chalice, Response
from psycopg2.extras import RealDictCursor
import psycopg2
import os
import requests
import hashlib
import json
import pdfkit
from jinja2 import Environment, FileSystemLoader

# DATABASE = {
#     'HOST': os.environ['DB_HOST'],
#     'PORT': os.environ['DB_PORT'],
#     'NAME': os.environ['DB_NAME'],
#     'USER': os.environ['DB_USER'],
#     'PASSWORD': os.environ['DB_PASSWORD'],
#     'MAPSBOX_API_KEY' : os.environ['MAPSBOX_API_KEY'],
#     'PREMIUM_API_KEY' : os.environ['PREMIUM_API_KEY'],
#     'BASIC_API_KEY' : os.environ['BASIC_API_KEY'],
#     'PREMIUM_COUNT' : os.environ['PREMIUM_COUNT'],
#     'PREMIUM_RADIUS' : os.environ['PREMIUM_RADIUS'],
#     'BASIC_COUNT' :  os.environ['BASIC_COUNT'],
#     'BASIC_RADIUS' : os.environ['BASIC_RADIUS']
# }

app = Chalice(app_name='terahome-vendor-api')

# db_host = DATABASE['HOST']
# db_port = DATABASE['PORT']
# db_name = DATABASE['NAME']
# db_user = DATABASE['USER']
# db_pass = DATABASE['PASSWORD']
db_host = "terahome-qa.ctxezg6na9wn.ca-central-1.rds.amazonaws.com"
db_port = "5432"
db_name = "terahome-qa"
db_user = "postgres"
db_pass = "teraHome12345#678"
map_box_key = "pk.eyJ1IjoidGVyYWhvbWUzNjAiLCJhIjoiY2tzc2J0MTdpMGVnOTMwcGpscTJzNTB4diJ9.FjMvbXk6d60omz5hahdUCA"
crypt_key = db_pass


def make_conn():
    conn = None
    try:
        conn = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (db_name, db_user, db_host, db_pass))
    except:
        print("I am unable to connect to the database")
    return conn


def fetch_data(conn, query):
    cursor = conn.cursor(cursor_factory=RealDictCursor)
    cursor.execute(query)
    raw = cursor.fetchall()
    cursor.close()
    return raw


class MapboxConfig:
    MAPSBOX_API_KEY = map_box_key


class Mapbox:
    def __init__(
            self,
            mapbox_api_key=MapboxConfig.MAPSBOX_API_KEY,

    ):
        self.mapbox_api_key = mapbox_api_key

    def get_travel_distance_and_time(
            self, origin_lat, origin_lon, destination_lat, destination_lon, mode
    ):
        url = f"https://api.mapbox.com/optimized-trips/v1/mapbox/{mode}/{origin_lon},{origin_lat};{destination_lon},{destination_lat}?access_token={self.mapbox_api_key}"
        payload = {}
        headers = {}
        response = requests.request("GET", url, headers=headers, data=payload)
        response_data = response.json()
        if "trips" in response_data and "legs" in response_data["trips"][0]:
            return response_data["trips"][0].get("legs")[0]
        return None


@app.route('/')
def index():
    return {'msg': 'vendor api'}


# !/usr/bin/python

@app.route('/nbh/{type}', methods=['GET'], cors=True)
def fetch_property(type):
    try:
        # import pdb
        # pdb.set_trace();
        facility_count = 3
        facility_radius = 2
        params = app.current_request.query_params
        lng = params.get("lng")
        lat = params.get("lat")
        point = "ST_MakePoint(" + lng + "," + lat + ")"
        query_cmd = "SELECT da_uid FROM neighbourhoods WHERE (ST_Contains(neighbourhoods.polygon,{}) OR ST_Contains(neighbourhoods.multipolygon, {}))".format(
            point, point)
        conn = make_conn()
        # get bar data
        nbh_result = fetch_data(conn, query_cmd)
        if nbh_result:
            dauid = nbh_result[0]["da_uid"]
            facility_data = {}
            if type == 'bar':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'bar',
                                                  'da_bars', 'bars_details')
            elif type == 'daycare':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'daycare',
                                                  'da_daycare', 'daycare_details')
            elif type == 'grocery':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'grocery',
                                                  'da_grocery', 'grocery_details')
            elif type == 'cafe':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'cafe',
                                                  'da_cafe', 'cafe_details')
            elif type == 'restaurant':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'restaurant',
                                                  'da_restaurants', 'restaurant_details')
            elif type == 'healthcare':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'healthcare',
                                                  'da_healthcare', 'healthcare_details')
            elif type == 'ele_school':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng,
                                                  'elementary school', 'da_school_ele', 'ele_school_details')
            elif type == 'sec_school':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng,
                                                  'secondary school', 'da_school_sec', 'sec_school_details')
            elif type == 'walkscore':
                facility_data = get_walkscore(conn, dauid)
            else:
                facility_data = {}

            conn.close()
            data = {
                type: facility_data,
            }
            return data
    except Exception as e:
        return str(e)


def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(loader=jinja2.FileSystemLoader(path or "./")).get_template(filename).render(context)


def get_data_from_api(facility_type):
    # import pdb
    # pdb.set_trace();
    api_url = f"https://hz3tqftb8e.execute-api.ca-central-1.amazonaws.com/api/nbh/auth/{facility_type}?lng=-79.3709183&api_key=FES80v8F3r4rMQfoFT7NC4q1sthviz9077SRfatI&lat=43.7639275&client_id=9562fbc00d56cc95209b818e8f9040654cecd9f4"
    response_data = requests.get(api_url)
    return (response_data.content)


@app.route("/get_pdf", methods=["GET"])
def get_pdf():
    # import pdb
    # pdb.set_trace();
    facility_count = 3
    facility_radius = 2
    conn = make_conn()
    curr_dir = os.getcwd()
    path_template = os.path.join(curr_dir, "templates")
    # path = "/home/gyan/Desktop/gyan1/projects/pdfApiParams/templates"
    all_types = ["bar", "daycare", "healthcare", "ele_school", "sec_school", "grocery", "cafe", "restaurant",
                 "walkscore"]
    # for all types of data dict/map
    data = {}
    conn = make_conn()
    facility_count = 3
    facility_radius = 2
    try:
        # data = get_facility_data("35200388", conn, facility_count, facility_radius, "-79.3709183","43.7639275",
        #                          'healthcare',
        #                          'da_healthcare', 'healthcare_details')
        for t in all_types:
            facility_type = t
            datas = get_data_from_api(facility_type)
            datas = json.loads(datas.decode('utf-8'))
            data[facility_type] = datas[facility_type]
        print(data)
    except:
        return {"msg": "unable to get all data from api"}

    # try:
    # import  pdb
    # pdb.set_trace();
    #  import pdb
    #  pdb.set_trace();
    path_wkhtmltopdf = 'C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=path_wkhtmltopdf)
    context = {'data': data}
    rendered = render("templates/index.html", context)
    css = ['templates/main.css']
    pdfkit.from_string(rendered, "output.pdf", css=css[0], configuration=config)
    # except:
    #     return {"msg": "some error in generating pdf"}
    # return all_data

    return all_data

    return {"msg": "pdf report generated successfully"}


@app.route('/nbh/auth/{type}', methods=['GET'], cors=True)
def fetch_property_with_auth(type):
    try:
        params = app.current_request.query_params
        api_key = params.get("api_key")
        client_id = params.get("client_id")
        if not client_id:
            return Response(body={'error': 'invalid client id'}, status_code=401)
        if not api_key:
            return Response(body={'error': 'invalid api key'}, status_code=401)

        conn = make_conn()
        vendor_query = f"SELECT api_key FROM vendors WHERE client_id = '{client_id}'"
        vendor_details = fetch_data(conn, vendor_query)
        if not len(vendor_details) > 0:
            return Response(body={'error': 'invalid client id'}, status_code=401)
        if not vendor_details[0]['api_key'] == api_key:
            return Response(body={'error': 'api key mismatch'}, status_code=400)

        if api_key == DATABASE['BASIC_API_KEY']:
            facility_count = int(DATABASE['BASIC_COUNT'])
            facility_radius = int(DATABASE['BASIC_RADIUS'])
        elif api_key == DATABASE['PREMIUM_API_KEY']:
            facility_count = int(DATABASE['PREMIUM_COUNT'])
            facility_radius = int(DATABASE['PREMIUM_RADIUS'])
        else:
            return Response(body={'error': 'api key mismatch'}, status_code=400)

        lng = params.get("lng")
        lat = params.get("lat")
        point = "ST_MakePoint(" + lng + "," + lat + ")"
        point = "ST_MakePoint(" + lng + "," + lat + ")"
        query_cmd = "SELECT da_uid FROM neighbourhoods"
        # query_cmd ="SELECT da_uid FROM neighbourhoods WHERE (ST_Contains(neighbourhoods.polygon,{}) OR ST_Contains(neighbourhoods.multipolygon, {}))".format(point,point)

        # get bar data
        nbh_result = fetch_data(conn, query_cmd)
        if nbh_result:
            dauid = nbh_result[0]["da_uid"]
            facility_data = {}
            if type == 'bar':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'bar',
                                                  'da_bars', 'bars_details')
            elif type == 'daycare':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'daycare',
                                                  'da_daycare', 'daycare_details')
            elif type == 'grocery':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'grocery',
                                                  'da_grocery', 'grocery_details')
            elif type == 'cafe':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'cafe',
                                                  'da_cafe', 'cafe_details')
            elif type == 'restaurant':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'restaurant',
                                                  'da_restaurants', 'restaurant_details')
            elif type == 'healthcare':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, 'healthcare',
                                                  'da_healthcare', 'healthcare_details')
            elif type == 'ele_school':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng,
                                                  'elementary school', 'da_school_ele', 'ele_school_details')
            elif type == 'sec_school':
                facility_data = get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng,
                                                  'secondary school', 'da_school_sec', 'sec_school_details')
            elif type == 'walkscore' and api_key == DATABASE['PREMIUM_API_KEY']:
                facility_data = get_walkscore(conn, dauid)
            else:
                facility_data = {}

            conn.close()
            data = {
                type: facility_data,
            }
            return Response(body=data, status_code=200)
    except Exception as e:
        return Response(body={'error': 'something went wrong'}, status_code=500)


def get_walkscore(conn, dauid):
    facility_query = "SELECT score_type,score_value FROM walkscore WHERE dauid = {}".format(dauid)
    facility_result = fetch_data(conn, facility_query)
    return facility_result


def get_facility_data(dauid, conn, facility_count, facility_radius, lat, lng, facility_name, facility_db,
                      facility_detail_db):
    refine_rating = 0
    if facility_name == 'daycare':
        facility_query = "SELECT id,rating FROM {} WHERE dauid = {}".format(facility_db, dauid)
        facility_result = fetch_data(conn, facility_query)
        refine_rating = facility_result[0]["rating"]
    else:
        facility_query = "SELECT id,refine_rating FROM {} WHERE dauid = {}".format(facility_db, dauid)
        facility_result = fetch_data(conn, facility_query)
        refine_rating = facility_result[0]["refine_rating"]

    # bar details using dauid for distance and time
    facility_detail_query = "SELECT name,address,destination_lat,destination_lon FROM {} WHERE dauid = {}".format(
        facility_detail_db, dauid)
    facility_details_result = fetch_data(conn, facility_detail_query)
    facilities_data = []
    facility_data = {}
    if facility_details_result:
        try:
            for facilities in facility_details_result:
                facility_lat = facilities["destination_lat"]
                facility_lng = facilities["destination_lon"]
                # distance calculation using mapbox API
                walk_details = Mapbox().get_travel_distance_and_time(lat, lng, facility_lat, facility_lng, "walking")

                walk_distance = round(walk_details['distance'] / 1000, 1)
                walk_time = round(walk_details['duration'] / 60)

                driving_details = Mapbox().get_travel_distance_and_time(lat, lng, facility_lat, facility_lng, "driving")
                drive_time = round(driving_details['duration'] / 60)
                drive_distance = round(driving_details['distance'] / 1000, 1)

                walk = {
                    "distance": walk_distance,
                    "time": walk_time,
                }
                drive = {
                    "distance": drive_distance,
                    "time": drive_time
                }
                facility = {
                    "name": facilities["name"],
                    "address": facilities["address"],
                    "walk": walk,
                    "drive": drive,
                    "coordinates": [facility_lat, facility_lng]
                }
                facilities_data.append(facility)
                # sorting facility by distance
                facilities_by_distance = sorted(facilities_data, key=lambda facility: facility["drive"]["distance"])
                # closest 3 or 5 facility based on API option
                closest_facility = facilities_by_distance[:facility_count]
                # facility withing given radius based on API option
                facility_withing_radius = [facility for facility in facilities_by_distance if
                                           facility["drive"]["distance"] <= facility_radius]

            facility_data = {
                "facility_rating": refine_rating,
                "facility_within_radius": str(len(facility_withing_radius)) + " {} within {} km".format(facility_name,
                                                                                                        facility_radius),
                "closest_facility": closest_facility,
            }
        except Exception as e:
            return str(e)

    return facility_data


@app.route('/vendor/create', methods=['POST'], cors=True)
def create_vendor():
    try:
        req_data = app.current_request.json_body
        name = req_data['name']
        email = req_data['email']
        subscription = req_data['subscription']
        organization = req_data['organization']
        if not name or not email or not organization:
            return Response(body={'error': 'please provice name/email/organization'}, status_code=400)
        encoded_email = email.encode('utf-8')
        hash_object = hashlib.sha1(encoded_email)
        client_id = hash_object.hexdigest()
        created_at = str(datetime.datetime.now())
        api_key = ''
        if subscription == 'basic':
            api_key = DATABASE['BASIC_API_KEY']
        elif subscription == 'premium':
            api_key = DATABASE['PREMIUM_API_KEY']
        else:
            return Response(body={'error': 'invalid subscription'}, status_code=401)

        query = f"SELECT email FROM vendors WHERE email = '{email}'"
        conn = make_conn()
        user_data = fetch_data(conn, query)
        if len(user_data) > 0:
            return Response(body={'error': 'vendor already exists with this email id'}, status_code=409)
        else:
            insert_query = """INSERT INTO vendors (created_at, name, email, client_id, api_key, organization, subscription, valid_till) VALUES(%s, %s, %s, %s, %s, %s, %s,%s)"""
            try:
                cursor = conn.cursor()
                cursor.execute(insert_query,
                               (created_at, name, email, client_id, api_key, organization, subscription, created_at))
                conn.commit()
                conn.close()
                data = {'message': 'vendor added', 'client_id': client_id, 'api_key': api_key}
                return Response(body=data, status_code=200)
            except Exception as e:
                return Response(body={'error': 'failed to create vendor'}, status_code=500)

    except Exception as e:
        return Response(body={'error': 'failed to create vendor'}, status_code=500)


# @app.route('/create/table')
def create_tables():
    """ create tables in the PostgreSQL database"""

    conn = None
    try:
        # connect to the PostgreSQL server
        conn = make_conn()
        cur = conn.cursor()
        # create table one by one
        cur.execute("DROP TABLE IF EXISTS vendors")
        cur.execute(
            "CREATE TABLE vendors(id SERIAL PRIMARY KEY, created_at VARCHAR(255), name VARCHAR(255), email VARCHAR(255), client_id VARCHAR(255), api_key VARCHAR(255),organization VARCHAR(255),subscription VARCHAR(255), valid_till VARCHAR(255))")

        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return True

